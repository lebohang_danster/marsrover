﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarsRoverLib
{
    public interface ICoordinate
    {
        int X { get; set; }
        int Y { get; set; }
        ICoordinate TurnRight();
        ICoordinate TurnLeft();
        ICoordinate Move();
        bool IsWithinBounds(Terrain terrain);
    }
}
