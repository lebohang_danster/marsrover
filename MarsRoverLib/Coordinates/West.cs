﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarsRoverLib
{
    public class West : ICoordinate
    {
        public West(int x, int y)
        {
            X = x;
            Y = y;
        }

        public int X
        {
            get; set;
        }

        public int Y
        {
            get; set;
        }

        public ICoordinate Move()
        {
            X--;
            return new West(X, Y);
        }

        public ICoordinate TurnLeft()
        {
            return new South(X, Y);
        }

        public ICoordinate TurnRight()
        {
            return new North(X, Y);
        }

        public override string ToString()
        {
            return Orientation.W.ToString();
        }

        public bool IsWithinBounds(Terrain terrain)
        {
            return (X - 1) >= 1;
        }
    }
}
