﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarsRoverLib
{
    public class South : ICoordinate
    {
        public South(int x, int y)
        {
            X = x;
            Y = y;
        }

        public int X
        {
            get; set;
        }

        public int Y
        {
            get; set;
        }

        public ICoordinate Move()
        {
            Y--;
            return new South(X, Y);
        }

        public ICoordinate TurnLeft()
        {
            return new East(X, Y);
        }

        public ICoordinate TurnRight()
        {
            return new West(X, Y);
        }

        public override string ToString()
        {
            return Orientation.S.ToString();
        }

        public bool IsWithinBounds(Terrain terrain)
        {
            return (Y - 1) >= 1;
        }
    }
}
