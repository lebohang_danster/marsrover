﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarsRoverLib
{
    public class North : ICoordinate
    {
        public North(int x, int y)
        {
            X = x;
            Y = y;
        }

        public int X
        {
            get; set;
        }

        public int Y
        {
            get; set;
        }

        public ICoordinate Move()
        {
            Y++;
            return new North(X, Y);
        }

        public ICoordinate TurnLeft()
        {
            return new West(X, Y);
        }

        public ICoordinate TurnRight()
        {
            return new East(X, Y);
        }

        public bool IsWithinBounds(Terrain terrain)
        {
            return (Y + 1) <= terrain.Y;
        }

        public override string ToString()
        {
            return Orientation.N.ToString();
        }

        
    }
}
