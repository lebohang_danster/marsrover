﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarsRoverLib
{
    public class Terrain
    {
        public int X { get; private set; }
        public int Y { get; private set; }

        public Terrain(int x, int y)
        {
            X = x;
            Y = y;
        }
    }
}
