﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarsRoverLib
{
    public class Rover
    {
        private Terrain Terrain { get; set; }
        private ICoordinate Coordinates { get; set; }

        public Rover (int x, int y, Orientation orientation, Terrain terrain)
        {
            switch (orientation)
            {
                case Orientation.E:
                    Coordinates = new East(x, y);
                    break;
                case Orientation.W:
                    Coordinates = new West(x, y);
                    break;
                case Orientation.N:
                    Coordinates = new North(x, y);
                    break;
                case Orientation.S:
                    Coordinates = new South(x, y);
                    break;
            }
            Terrain = terrain;
        }

        public void ExecuteCommands(string commands)
        {
            foreach (char command in commands)
            {
                var roverCommand = (Command)Enum.Parse(typeof(Command), command.ToString());
                switch (roverCommand)
                {
                    case Command.R:
                        Coordinates = Coordinates.TurnRight();
                        break;
                    case Command.L:
                        Coordinates = Coordinates.TurnLeft();
                        break;
                    case Command.M:
                        if (Coordinates.IsWithinBounds(Terrain))
                            Coordinates = Coordinates.Move();
                        break;
                }
            }
        }

        public string GetOrientation()
        {
            return Coordinates.ToString();
        }

        public int GetXCoordinate()
        {
            return Coordinates.X;
        }

        public int GetYCoordinate()
        {
            return Coordinates.Y;
        }

        public override string ToString()
        {
            return string.Format("{0} {1} {2}", 
                GetXCoordinate(), 
                GetYCoordinate(), 
                GetOrientation());
        }
    }
}
