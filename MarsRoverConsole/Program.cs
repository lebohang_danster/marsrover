﻿using MarsRoverLib;
using System.Configuration;

namespace MarsRoverConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            var filePath = ConfigurationManager.AppSettings["filePath"];
            RoverFileReader fileReader = RoverFileReader.BuildFileReader(filePath);

            var terrain = new Terrain(fileReader.SurfaceX, fileReader.SurfaceY);
            Rover rover = new Rover(fileReader.XCoordinate, fileReader.YCoordinate, fileReader.Orientation, terrain);
            rover.ExecuteCommands(fileReader.Commands);


            System.Console.WriteLine(rover.ToString());
            System.Console.ReadLine();
        }
    }
}
