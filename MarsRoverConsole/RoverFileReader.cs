﻿using System;
using System.IO;

namespace MarsRoverConsole
{
    public class RoverFileReader
    {
        public int XCoordinate { get; private set; }
        public int YCoordinate { get; private set; }
        public Orientation Orientation { get; private set; }
        public int SurfaceX { get; private set; }
        public int SurfaceY { get; private set; }
        public string Commands { get; private set; }

        private RoverFileReader() {}

        public static RoverFileReader BuildFileReader(string filePath)
        {
            var fileReader = new RoverFileReader();
            
            int lineCount = 1;
            foreach (var lineItem in File.ReadLines(filePath))
            {
                switch (lineCount)
                {
                    case 1:
                        var surfaceCoords = lineItem.Split(new []{ ' ' });
                        fileReader.SurfaceX = int.Parse(surfaceCoords[0]);
                        fileReader.SurfaceY = int.Parse(surfaceCoords[1]);
                        break;

                    case 2:
                        var coords = lineItem.Split(new[] { ' ' });
                        fileReader.XCoordinate = int.Parse(coords[0]);
                        fileReader.YCoordinate = int.Parse(coords[1]);
                        fileReader.Orientation = (Orientation)Enum.Parse(typeof(Orientation),coords[2]);
                        break;

                    case 3:
                        fileReader.Commands = lineItem;
                        break;
                }
                lineCount++;
            }
            return fileReader;
            
        }

    }
}
