# README #

## Summary of set up
This README is for the Mars Rover Challenge. The challenge is written in C# and requires Visual Studio to build. I recommend that you use Visual Studio 2015. After building the solution, it can be ran by either double clicking the .exe file in the bin directory or you can run it straight from your IDE.

## Configuration
You have to configure the location of the file in the app.config 

## Example configuration
<add key="filePath" value="C:\Users\lebohang\Documents\test.txt"/>

Then you should be ready to run the application

# Sample commands for the application:
8 8

1 2 E

MMMLMRMMRRMML