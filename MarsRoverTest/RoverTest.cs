﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MarsRoverLib;

namespace MarsRoverTest
{
    [TestClass]
    public class RoverTest
    {
        [TestMethod]
        public void TestRoverChangesDirectionToEastFromNorthWhenRecievingRCommand()
        {
            var rover = new Rover(1, 1, Orientation.N, null);
            rover.ExecuteCommands("R");
            Assert.AreEqual("E", rover.GetOrientation());
        }

        [TestMethod]
        public void TestRoverChangesDirectionToWestFromNorthWhenRecievingLCommand()
        {
            var rover = new Rover(1, 1, Orientation.N, null);
            rover.ExecuteCommands("L");
            Assert.AreEqual("W", rover.GetOrientation());
        }

        [TestMethod]
        public void TestRoverMovingIncreasesYCoordinateWhenFacingNorth()
        {
            var rover = new Rover(1, 1, Orientation.N, new Terrain(4, 4));
            rover.ExecuteCommands("M");
            Assert.AreEqual(2, rover.GetYCoordinate());
            Assert.AreEqual(1, rover.GetXCoordinate());
        }

        [TestMethod]
        public void TestRoverMovingDecreasesYCoordinateWhenFacingSouth()
        {
            var rover = new Rover(1, 2, Orientation.S, null);
            rover.ExecuteCommands("M");
            Assert.AreEqual(1, rover.GetYCoordinate());
            Assert.AreEqual(1, rover.GetXCoordinate());
        }

        [TestMethod]
        public void TestRoverMovingIncreasesXCoordinateWhenFacingEast()
        {
            var rover = new Rover(1, 1, Orientation.E, new Terrain(4, 4));
            rover.ExecuteCommands("M");
            Assert.AreEqual(1, rover.GetYCoordinate());
            Assert.AreEqual(2, rover.GetXCoordinate());
        }

        [TestMethod]
        public void TestRoverMovingDecreasesXCoordinateWhenFacingWest()
        {
            var rover = new Rover(2, 1, Orientation.W, null);
            rover.ExecuteCommands("M");
            Assert.AreEqual(1, rover.GetYCoordinate());
            Assert.AreEqual(1, rover.GetXCoordinate());
        }

        [TestMethod]
        public void TestRoverPositionAndDirectionIsCorrectWhenExcutingCommands()
        {
            var rover = new Rover(1, 2, Orientation.E, new Terrain(8, 8));
            rover.ExecuteCommands("MMLMRMMRRMML");
            Assert.AreEqual(3, rover.GetXCoordinate());
            Assert.AreEqual(3, rover.GetYCoordinate());
            Assert.AreEqual("S", rover.GetOrientation());
            Assert.AreEqual("3 3 S", rover.ToString());
        }
    }
}
